#!/bin/bash
##
## WGBS analysis of already qced-cutadapted-trimmed pe data using bwa-meth and methyldackel
##
## Izaskun Mallona
## 8th June 2018
## GPL

export HOME=/home/ubuntu
export TASK="wgbs"
export WD="$HOME"/"$TASK"

export SOFT="$HOME"/soft
export VIRTENVS="$HOME"/virtenvs
export FASTQC=/usr/bin/fastqc
export SICKLE="$HOME"/soft/sickle/sickle-1.33/sickle
export CUTADAPT="$VIRTENVS"/cutadapt/bin/cutadapt
export QUALIMAP="$SOFT"/qualimap/qualimap_v2.2.1/qualimap
export METHYLDACKEL="$SOFT"/methyldackel/MethylDackel/MethylDackel
export PICARD="$SOFT"/picard/build/libs/picard.jar
export BEDTOOLS="$SOFT"/bedtools/bin/bedtools

export MM9=/home/ubuntu/REPOS/annotation/mm9.fa
export NTHREADS=8
export MAPQ_THRES=40

export ILLUMINA_UNIVERSAL="AGATCGGAAGAG"
export ILLUMINA="CGGTTCAGCAGGAATGCCGAGATCGGAAGAGCGGTT"

export SAMPLE=20180523.A-WGBS_LSH_6weeks_10cycl
export FW="$HOME"/MOUNT/NI/WGBS_PE/"$SAMPLE"_R1.fastq.gz
export RV="$HOME"/MOUNT/NI/WGBS_PE/"$SAMPLE"_R2.fastq.gz


mkdir -p $WD

cd $_

mysql --user=genome \
      --host=genome-mysql.cse.ucsc.edu -A -e "select chrom, size from mm9.chromInfo" > mm9.genome

# echo sampling

# zcat $FW | head -10000 | gzip -c > "$(basename $FW)".mini.gz
# zcat $RV | head -10000 | gzip -c > "$(basename $RV)".mini.gz

# source $VIRTENVS/bwa-meth/bin/activate

# ( bwameth.py --reference "$MM9" \
#              "$(basename $FW)".mini.gz "$(basename $RV)".mini.gz \
#              --threads $NTHREADS |  \
#         samtools view -bS - | \
#         samtools sort  - > \
#                  "$SAMPLE"_bwameth_default.bam ) \
#     3>&1 1>&2 2>&3 | tee "$SAMPLE"_bwameth_default.log

# deactivate

echo "$date" bwameth start

source $VIRTENVS/bwa-meth/bin/activate

( bwameth.py --reference "$MM9" \
             "$FW" "$RV"
             --threads $NTHREADS |  \
        samtools view -bS - | \
        samtools sort  - > \
                 "$SAMPLE"_bwameth_default.bam ) \
    3>&1 1>&2 2>&3 | tee "$SAMPLE"_bwameth_default.log

deactivate


echo "$date" bwameth end

bam="$SAMPLE"_bwameth_default.bam

echo "$date" qualimap start

"$QUALIMAP" bamqc \
            -bam "$bam" \
            -gd mm9 \
            -outdir "$(basename $bam .bam)"_qualimap \
            --java-mem-size=10G \
            -nt "$NTHREADS"

echo "$date" qualimap end

echo "$date" markduplicates start

java -jar -XX:ParallelGCThreads=$NTHREADS \
     "$PICARD" MarkDuplicates INPUT=$WD/"$bam" \
     REMOVE_DUPLICATES=TRUE \
     REMOVE_SEQUENCING_DUPLICATES=TRUE \
     OUTPUT=$WD/"$(basename $bam .bam)""_dup_marked.bam" \
     METRICS_FILE=$WD/"$(basename $bam .bam)""_dup_marked.metrics"

echo "$date" markduplicates end

echo "$date" methyldackel start

"$METHYLDACKEL" extract \
                -q "$MAPQ_THRES" \
                -@ "$NTHREADS" \
                "$MM9" \
                "$bam" \
                -o "$(basename $bam .bam)"

echo "$date" methyldackel end
