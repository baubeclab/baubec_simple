#!/bin/bash
##
## WGBS analysis of pe data using bwa-meth and methyldackel
##
## Izaskun Mallona
## 8th June 2018
## GPL

export HOME=/home/imallona
export TASK="wgbs_nina"
export WD="$HOME"/"$TASK"
export DATA="$HOME"/mnt/baubec

export SOFT="$HOME"/soft
export MM9=/home/Shared/data/annotation/Mouse/mm9/mm9.fa
export VIRTENVS=~/virtenvs

export NTHREADS=6
export MAPQ_THRES=40

export FASTQC=/usr/local/software/FastQC/fastqc
export SICKLE="$HOME"/soft/sickle/sickle-1.33/sickle
export CUTADAPT="$VIRTENVS"/cutadapt/bin/cutadapt
export QUALIMAP="$SOFT"/qualimap/qualimap_v2.2.1/qualimap
export METHYLDACKEL="$SOFT"/methyldackel/MethylDackel/MethylDackel
export MARKDUPLICATES=/usr/local/software/picard-tools-1.96/MarkDuplicates.jar
export BEDTOOLS="$SOFT"/bedtools/bin/bedtools
export FASTQDUMP=/usr/local/software/sratoolkit.2.9.0-ubuntu64/bin/fastq-dump

export ILLUMINA_UNIVERSAL="AGATCGGAAGAG"
export ILLUMINA="CGGTTCAGCAGGAATGCCGAGATCGGAAGAGCGGTT"

export SAMPLE=20180523.A-WGBS_LSH_6weeks_10cycl
export FW="$SAMPLE"_R1.fastq.gz
export RV="$SAMPLE"_R2.fastq.gz


mkdir -p $WD

cd $_

mysql --user=genome \
      --host=genome-mysql.cse.ucsc.edu -A -e "select chrom, size from mm9.chromInfo" > mm9.genome

# echo sampling

# zcat $FW | head -10000 | gzip -c > "$(basename $FW)".mini.gz
# zcat $RV | head -10000 | gzip -c > "$(basename $RV)".mini.gz

# source $VIRTENVS/bwa-meth/bin/activate

# ( bwameth.py --reference "$MM9" \
#              "$(basename $FW)".mini.gz "$(basename $RV)".mini.gz \
#              --threads $NTHREADS |  \
#         samtools view -bS - | \
#         samtools sort  - > \
#                  "$SAMPLE"_bwameth_default.bam ) \
#     3>&1 1>&2 2>&3 | tee "$SAMPLE"_bwameth_default.log

# deactivate

sample="$SAMPLE"

for r in 1 2
do
    curr=${WD}/${sample}_"$r"
    mkdir -p $curr
    
    $FASTQC ${DATA}/"$sample"_R"$r".fastq.gz --outdir ${curr} \
            -t $NTHREADS &> ${curr}/${sample}_fastqc.log

done

source $VIRTENVS/cutadapt/bin/activate

cutadapt \
    -j $NTHREADS \
    -b $ILLUMINA_UNIVERSAL -b $ILLUMINA \
    -B $ILLUMINA_UNIVERSAL -B $ILLUMINA \
    -o "$WD"/"${sample}"_1_cutadapt.fastq.gz \
    -p "$WD"/"${sample}"_2_cutadapt.fastq.gz \
    "$DATA"/"$sample"_R1.fastq.gz "$DATA"/"$sample"_R2.fastq.gz &> "$WD"/"$sample"_cutadapt.log

deactivate


"$SICKLE" pe \
          -f "$WD"/"$sample"_1_cutadapt.fastq.gz \
          -r "$WD"/"$sample"_2_cutadapt.fastq.gz \
          -o "$WD"/"$sample"_1_cutadapt_sickle.fastq.gz \
          -p "$WD"/"$sample"_2_cutadapt_sickle.fastq.gz \
          -t sanger \
          -s "$WD"/"$sample"_cutadapt_sickle_singles.fastq.gz \
          -g &> "$WD"/"$sample"_cutadapt_sickle.log


rm -f "$WD"/"${sample}"_1_cutadapt.fastq.gz "$WD"/"${sample}"_2_cutadapt.fastq.gz

for r in 1 2
do
    curr="$sample"_"$r"_cutadapt_sickle
    mkdir -p "$WD"/"$curr"
    $FASTQC "$WD"/${sample}_"$r"_cutadapt_sickle.fastq.gz \
            --outdir "$WD"/"$curr" \
            -t $NTHREADS &> ${curr}/${sample}_"$r"_fastqc.log
done    

echo "$date" bwameth start

source $VIRTENVS/bwa-meth/bin/activate

( bwameth.py --reference "$MM9" \
             "$WD"/"${sample}"_1_cutadapt_sickle.fastq.gz "$WD"/"${sample}"_2_cutadapt_sickle.fastq.gz \
             --threads $NTHREADS |  \
        samtools view -@ $NTHREADS -bS - > \
                 "$SAMPLE"_bwameth_default.bam ) \
    3>&1 1>&2 2>&3 | tee "$SAMPLE"_bwameth_default.log

deactivate

echo "$date" bwameth end

echo "$date" sorting start

bam="$SAMPLE"_bwameth_default.bam

samtools sort -@ $NTHREADS "$bam" > "$bam".sorted
mv -f "$bam".sorted "$bam"

echo "$date" sorting end

echo "$date" qualimap start

"$QUALIMAP" bamqc \
            -bam "$bam" \
            -gd mm9 \
            -outdir "$(basename $bam .bam)"_qualimap \
            --java-mem-size=10G \
            -nt "$NTHREADS"

echo "$date" qualimap end

echo "$date" markduplicates start

# java -jar -XX:ParallelGCThreads=$NTHREADS \
#      "$MARKDUPLICATES" INPUT=$WD/"$bam" \
#      REMOVE_DUPLICATES=TRUE \
#      REMOVE_SEQUENCING_DUPLICATES=TRUE \
#      OUTPUT=$WD/"$(basename $bam .bam)""_dup_marked.bam" \
#      METRICS_FILE=$WD/"$(basename $bam .bam)""_dup_marked.metrics"

java -jar -XX:ParallelGCThreads=$NTHREADS \
     "$MARKDUPLICATES" INPUT=$WD/"$bam" \
     REMOVE_DUPLICATES=TRUE \
     OUTPUT=$WD/"$(basename $bam .bam)""_dup_marked.bam" \
     METRICS_FILE=$WD/"$(basename $bam .bam)""_dup_marked.metrics"

echo "$date" markduplicates end

echo "$date" methyldackel start

"$METHYLDACKEL" extract \
                -q "$MAPQ_THRES" \
                -@ "$NTHREADS" \
                "$MM9" \
                "$bam" \
                -o "$(basename $bam .bam)"

echo "$date" methyldackel end
